package com.laurapereyra.springaccount.api;

import com.laurapereyra.springaccount.bl.AccountBl;
import com.laurapereyra.springaccount.dto.AccountDto;
import com.laurapereyra.springaccount.dto.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping
public class AccountController {

    AccountBl accountBl;

    @Autowired
    public AccountController(AccountBl accountBl) {
        this.accountBl = accountBl;
    }

    @RequestMapping(path = "/account-list/{customerId}",
                    method = RequestMethod.GET,
                    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ResponseDto<AccountDto>> findAccount(
            @PathVariable int customerId){
        System.out.println("Recibiendo el id: " + customerId);
        ResponseDto<AccountDto> result;
        List<AccountDto> accountDto = accountBl.getCustomerAccountById(customerId);
        if (accountDto!= null){
            result = new ResponseDto("MS-0000",accountDto,null);
            System.out.println(result);
            return new ResponseEntity<>(result, HttpStatus.OK);
        }else{
            result = new ResponseDto<>("MS-0404",null,"El cliente no tiene cuentas");
            System.out.println(result);
            return new ResponseEntity<>(result, HttpStatus.NOT_FOUND);
        }

    }




}
