package com.laurapereyra.springaccount.mapper;

import com.laurapereyra.springaccount.dto.AccountDto;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface AccountMapper {
    @Select("select a.account_id, a.type_account, a.account_number, a.amount_a, " +
            "a.amount_d, a.pledged, a.customer_id \n" +
            "from account a, customer c \n" +
            "where a.customer_id = #{customerId}\n" +
            "and a.customer_id = c.customer_id")
    List<AccountDto> getCustomerAccountById(@Param("customerId") int customerId);
}
