package com.laurapereyra.springaccount.dto;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ResponseDto<T> {
    private String statusCode;
    private T response;
    private String errorDetail;

    public ResponseDto(String statusCode, T response, String errorDetail) {
        this.statusCode = statusCode;
        this.response = response;
        this.errorDetail = errorDetail;
    }
}
