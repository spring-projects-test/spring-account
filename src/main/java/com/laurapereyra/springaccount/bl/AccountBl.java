package com.laurapereyra.springaccount.bl;

import com.laurapereyra.springaccount.dto.AccountDto;
import com.laurapereyra.springaccount.mapper.AccountMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class AccountBl {
    private AccountMapper accountMapper;

    @Autowired
    public AccountBl(AccountMapper accountMapper) {
        this.accountMapper = accountMapper;
    }

    public List<AccountDto> getCustomerAccountById(int customerId){
        return accountMapper.getCustomerAccountById(customerId);
    }
}
